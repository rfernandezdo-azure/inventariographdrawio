$urlimgi = "https://gitlab.com/rfernandezdo-azure/stencils/raw/master/iconset"
$urlimgce = "https://gitlab.com/rfernandezdo-azure/stencils/raw/master/Microsoft_CloudnEnterprise_Symbols"
$file = 'drawio-azure-import.csv'
########## REDES
$query = "where type =~ 'Microsoft.Network/virtualNetworks'
| extend subnetsid = aliases ['Microsoft.Network/virtualNetworks/subnets[*].id']
| project name, type, resourceGroup, location, subscriptionId,id,properties,
fill='#dae8fc',stroke='#6c8ebf',refs=subnetsid,image='$urlimgi/Networking%20Service%20Color/Virtual%20Networks.svg'"
Search-AzGraph -Query $query | Export-Csv -NoTypeInformation -Path $file

######### SUBNETS
$vnets = Search-AzGraph -Query "where type =~ 'Microsoft.Network/virtualNetworks' | project name" 
ForEach ($vnet in $vnets.name) {
    $subnets = Search-AzGraph -Query "where type =~ 'Microsoft.Network/virtualNetworks' and name=='$vnet' 
    | extend name = aliases ['Microsoft.Network/virtualNetworks/subnets[*].name']
    | project name"
    Write-Debug $subnets
    For ($i=0; $i -lt $subnets.name.Length; $i++) {
        $query = "where type =~ 'Microsoft.Network/virtualNetworks' and name=='$vnet'
        | project name = properties.subnets[$i].name, type = properties.subnets[$i].type, resourceGroup, location, subscriptionId,id=properties.subnets[$i].id,properties,
        fill='#dae8fc',stroke='#6c8ebf',refs= ' ',image='$urlimgce/BONUS/AzurePortaliconsDump/Service_related/Subnet.svg',ResourceId=properties.subnets[$i].id"
        Search-AzGraph -Query $query | Export-csv -Append -Path $file
    }
}

######### NICS
$query = "where type =~ 'Microsoft.Network/networkInterfaces'
| extend subnetsref = aliases ['Microsoft.Network/networkInterfaces/ipconfigurations[*].subnet.id']
| extend vmsref = aliases ['Microsoft.Network/networkInterfaces/virtualMachine.id']
| project name, type, resourceGroup, location, subscriptionId,id,properties,
fill='#dae8fc',stroke='#6c8ebf',refs=strcat(subnetsref,' ',vmsref),url='url',image='$urlimgi/Networking%20Service%20Color/Network%20Interfaces.svg'"
Search-AzGraph -Query $query  | Export-csv -Append -Path $file

######### VMS
$query = "where type =~ 'Microsoft.Compute/virtualMachines'
| project name, type, resourceGroup, location, subscriptionId,id,properties,
fill='#dae8fc',stroke='#6c8ebf',refs=properties.availabilitySet.id,url='url',image='$urlimgi/Compute%20Service%20Color/VM/VM.svg'"
Search-AzGraph -Query $query  | Export-csv -Append -Path $file

######## Disks
$query = "where type =~ 'Microsoft.Compute/disks'
| project name, type, resourceGroup, location, subscriptionId,id,properties,
fill='#dae8fc',stroke='#6c8ebf',refs=managedBy,url='url',image='$urlimgi/Compute%20Service%20Color/Disks.svg'"
Search-AzGraph -Query $query | Export-csv -Append -Path $file

######## Availability Sets
$query = "where type =~ 'Microsoft.Compute/availabilitySets'
| project name, type, resourceGroup, location, subscriptionId,id,properties,
fill='#dae8fc',stroke='#6c8ebf',refs=' ',url='url',image='$urlimgi/Compute%20Service%20Color/VM/Availability%20Sets.svg'"
Search-AzGraph -Query $query | Export-csv -Append -Path $file

#Ajustes en refs

$patternspace = '. .'
$patternbrackets = '[.]' 
Import-Csv -Path $file | ForEach-Object {
#Sustituir [ y ] por nada
    If ($_.refs -match $patternbrackets) {
        $aux=$_.refs
        $_.refs = $aux | %{$_ -replace '\["',''} | %{$_ -replace '"\]',''}
    }
#Añadir una coma en refs si existen varias referencias    
    If ($_.refs -match $patternspace ) {
        $aux=$_.refs
        $_.refs = $aux | %{$_ -replace " ",","} | %{$_ -replace '"',''}
         }
         $_
     } | Export-Csv -NoTypeInformation -Path final-$file -Force